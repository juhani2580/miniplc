Program = "program" Id ";" { Procedure | Function } MainBlock "."
Procedure = "procedure" Id "(" Parameters ")" ";" Block ";"
Function = "function" Id "(" Parameters ")" ":" Type ";" Block ";"
VarDecl = "var" Id { "," Id } ":" Type
Parameters = [ [ "var" ] Id ":" Type { "," [ "var" ] Id ":" Type } ]
Type = SimpleType | ArrayType
ArrayType = "array" "[" [ IntegerExpr ] "]" "of" SimpleType
SimpleType = TypeId
Block = "begin" Stmt { ";" Stmt } [ ";" ] "end"
Stmt = SimpleStmt | StructuredStmt | VarDecl

SimpleStmt = AssignStmt | Call | ReturnStmt | ReadStmt | WriteStmt | AssertStmt
AssignStmt = Variable ":=" Expr
Call = Id "(" Args ")"
Args = [ Expr { "," Expr } ]
ReturnStmt = "return" [ Expr ]
ReadStmt = "read" "(" Variable { "," Variable } ")"
WriteStmt = "writeln" "(" Args ")"
AssertStmt = "assert" "(" BoolExpr ")"

StructuredStmt = Block | IfStmt | WhileStmt
IfStmt = "if" BoolExpr "then" Stmt "else" Stmt
       = "if" BoolExpr "then" Stmt
WhileStmt = "while" BoolExpr "do" Stmt
Expr = SimpleExpr | SimpleExpr RelOp SimpleExpr
SimpleExpr = [ Sign ] Term { AddOp Term }
Term = Factor { MulOp Factor }
Factor =
    Call | Variable | Literal | "(" Expr ")" | "not" Factor | Factor "." "size"
Variable = VariableId [ "[" IntegerExpr "]" ]

RelOp = "=" | "<>" | "<" | "<=" | ">=" | ">"
SignOp = "+" | "-"
NegOp = "not"
AddOp = "+" | "-" | "or"
MulOp = "*" | "/" | "%" | "and"

!!! Lexical grammar

Id = Letter { Letter | Digit | "_" }
Literal = IntegerLiteral | RealLiteral | StringLiteral
IntegerLiteral = Digits
Digits = Digit { Digit }
RealLiteral = Digits "." Digits [ "e" [ Sign ] Digits ]
StringLiteral = "\"" { ACharOrEscapeChar } "\""
Letter =
    "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" |
    "m" | "n" | "o" | "p" | "q" | "r" | "s" | "t" | "u" | "v" | "w" | "x" |
    "y" | "z" | "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J" |
    "K" | "L" | "M" | "N" | "O" | "P" | "Q" | "R" | "S" | "T" | "U" | "V" |
    "W" | "X" | "Y" | "Z"
Digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
SpecialSymbolOrKeyword =
    "+" | "-" | "*" | "%" | "=" | "<>" | "<" | ">" | "<=" | ">=" |
    "(" | ")" | "[" | "]" | ":=" | "." | "," | ";" | ":" | "or" |
    "and" | "not" | "if" | "then" | "else" | "of" | "while" | "do" |
    "begin" | "end" | "var" | "array" | "procedure" |
    "function" | "program" | "assert" | "return"
PredefinedId =
    "Boolean | "false" | "integer" | "read" | "real" | "size" |
    "string" | "true" | "writeln"
