﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
namespace Minipl
{
public struct Position
{
    public int Line;
    public int Col;
    public override string ToString()
    {
        return string.Format("{0}:{1}", Line, Col);
    }
}
public enum Tag
{
    StringLiteral,
    IntegerLiteral,
    RealLiteral,
    Keyword,
    Identifier,
    Error,
    Eof,
}
public class Scanner
{
    public Tag Tag;
    public Position Position;
    public string String;
    public int Integer;
    public double Real;
    public Scanner(System.IO.TextReader reader)
    {
        _Reader = reader;
        _Sb = new System.Text.StringBuilder();
    }
    public override string ToString()
    {
        String s = string.Format(
            "{0}: {1}",
            Position,
            Tag.ToString());
        if (Tag == Tag.StringLiteral)
        {
            return string.Format("{0}: \"{1}\"", s, String);
        }
        if (Tag == Tag.IntegerLiteral)
        {
            return string.Format("{0}: {1}", s, Integer);
        }
        else if (Tag == Tag.RealLiteral)
        {
            return string.Format("{0}: {1}", s, Real);
        }
        return string.Format("{0}: {1}", s, String);
    }
    public Tag Scan()
    {
    Again:
        Position.Line = _Line;
        Position.Col = _Col;
        while (_IsWhiteSpace(_Peek))
        {
            _Read();
        }
        if (_Peek == '\0')
        {
            _Read();
            Tag = Tag.Eof;
        }
        else if (_Peek == '0' ||
                 _Peek == '1' ||
                 _Peek == '2' ||
                 _Peek == '3' ||
                 _Peek == '4' ||
                 _Peek == '5' ||
                 _Peek == '6' ||
                 _Peek == '7' ||
                 _Peek == '8' ||
                 _Peek == '9')
        {
            _Sb.Clear();
            _Sb.Append(_Read());
            while (_Peek == '0' ||
                   _Peek == '1' ||
                   _Peek == '2' ||
                   _Peek == '3' ||
                   _Peek == '4' ||
                   _Peek == '5' ||
                   _Peek == '6' ||
                   _Peek == '7' ||
                   _Peek == '8' ||
                   _Peek == '9')
            {
                _Sb.Append(_Read());
            }
            if(_Peek == '.')
            {
                _Read();
                _Sb.Append(_Read());
                while (_Peek == '0' ||
                       _Peek == '1' ||
                       _Peek == '2' ||
                       _Peek == '3' ||
                       _Peek == '4' ||
                       _Peek == '5' ||
                       _Peek == '6' ||
                       _Peek == '7' ||
                       _Peek == '8' ||
                       _Peek == '9' ||
                       _Peek == 'e' ||
                       _Peek == '+' ||
                       _Peek == '-')
                {

                }
                Real = Double.Parse(_Sb.ToString());
                Tag = Tag.RealLiteral;
            }
            else
            {
                Integer = Int32.Parse(_Sb.ToString());
                Tag = Tag.IntegerLiteral;
            }
        }
        else if(_Peek == '"')
        {
            _Read();
            _Sb.Clear();
            while (!(_Peek == '"' || _Peek == '\0'))
            {
                _Sb.Append(_ReadReserveCase());
            }
            if (_Read() == '"')
            {
                String = _Sb.ToString();
                String = String
                    .Replace("\\\\", "\\")
                    .Replace("\\n", "\n")
                    .Replace("\\t", "\t");
                Tag = Tag.StringLiteral;
            }
            else
            {
                String = "expected \"";
                Tag = Tag.Error;
            }
        }
        else if(_Peek == '{')
        {
            _Read();
            if (_Read() != '*')
            {
                String = "expected '*'";
                Tag = Tag.Error;
            }
            else
            {
                for(;;)
                {
                    char c = _Read();
                    if (c == '\0')
                    {
                        break;
                    }
                    if (c == '*' && _Read() == '}')
                    {
                        goto Again;
                    }
                }
                String = "expected \"*}\"";
                Tag = Tag.Error;
            }
        }
        else if(_Peek == '+' ||
                _Peek == '-' ||
                _Peek == '*' ||
                _Peek == '%' ||
                _Peek == '=' ||
                _Peek == '<' ||
                _Peek == '>' ||
                _Peek == '(' ||
                _Peek == ')' ||
                _Peek == '[' ||
                _Peek == ']' ||
                _Peek == '.' ||
                _Peek == ',' ||
                _Peek == ':' ||
                _Peek == ';')
        {
            String = _Read().ToString();
            if(_Peek == '>' || _Peek == '=')
            {
                String += _Read();
            }
            Tag = Tag.Keyword;
        }
        else if (_Peek == 'a' ||
                 _Peek == 'b' ||
                 _Peek == 'c' ||
                 _Peek == 'd' ||
                 _Peek == 'e' ||
                 _Peek == 'f' ||
                 _Peek == 'g' ||
                 _Peek == 'h' ||
                 _Peek == 'i' ||
                 _Peek == 'j' ||
                 _Peek == 'k' ||
                 _Peek == 'l' ||
                 _Peek == 'm' ||
                 _Peek == 'n' ||
                 _Peek == 'o' ||
                 _Peek == 'p' ||
                 _Peek == 'q' ||
                 _Peek == 'r' ||
                 _Peek == 's' ||
                 _Peek == 't' ||
                 _Peek == 'u' ||
                 _Peek == 'v' ||
                 _Peek == 'w' ||
                 _Peek == 'x' ||
                 _Peek == 'y' ||
                 _Peek == 'z')
        {
            _Sb.Clear();
            do
            {
                _Sb.Append(_Read());
            } while  (_Peek == 'a' ||
                      _Peek == 'b' ||
                      _Peek == 'c' ||
                      _Peek == 'd' ||
                      _Peek == 'e' ||
                      _Peek == 'f' ||
                      _Peek == 'g' ||
                      _Peek == 'h' ||
                      _Peek == 'i' ||
                      _Peek == 'j' ||
                      _Peek == 'k' ||
                      _Peek == 'l' ||
                      _Peek == 'm' ||
                      _Peek == 'n' ||
                      _Peek == 'o' ||
                      _Peek == 'p' ||
                      _Peek == 'q' ||
                      _Peek == 'r' ||
                      _Peek == 's' ||
                      _Peek == 't' ||
                      _Peek == 'u' ||
                      _Peek == 'v' ||
                      _Peek == 'w' ||
                      _Peek == 'x' ||
                      _Peek == 'y' ||
                      _Peek == 'z' ||
                      _Peek == '0' ||
                      _Peek == '1' ||
                      _Peek == '2' ||
                      _Peek == '3' ||
                      _Peek == '4' ||
                      _Peek == '5' ||
                      _Peek == '6' ||
                      _Peek == '7' ||
                      _Peek == '8' ||
                      _Peek == '9' ||
                      _Peek == '_');
            String = _Sb.ToString();
            if (String == "or" ||
                String == "and" ||
                String == "not" ||
                String == "if" ||
                String == "then" ||
                String == "else" ||
                String == "of" ||
                String == "while" ||
                String == "do" ||
                String == "begin" ||
                String == "end" ||
                String == "var" ||
                String == "array" ||
                String == "procedure" ||
                String == "function" ||
                String == "program" ||
                String == "return")
            {
                Tag = Tag.Keyword;
            }
            else
            {
                Tag = Tag.Identifier;
            }
        }
        else
        {
            _Read();
            String = "unknown token";
            Tag = Tag.Error;
        }
        return Tag;
    }
    System.IO.TextReader _Reader;
    System.Text.StringBuilder _Sb;
    int _Line = 0;
    int _Col = 0;
    char _ReadReserveCase()
    {
        int c = _Reader.Read();
        if (c == -1)
        {
            return '\0';
        }
        char ret = (char)c;
        if (ret == '\n')
        {
            _Line++;
            _Col = 0;
        }
        else
        {
            _Col++;
        }
        return ret;
    }
    char _Read()
    {
        return Char.ToLower(_ReadReserveCase());
    }
    char _Peek
    {
        get
        {
            int c = _Reader.Peek();
            if (c == -1)
            {
                return '\0';
            }
            return Char.ToLower((char)c);
        }
    }
    bool _IsWhiteSpace(char c)
    {
        if (c == ' ' ||
            c == '\t' ||
            c == '\n' ||
            c == '\r')
        {
            return true;
        }
        return false;
    }
}
public abstract class Ast
{
    public Position Position;
    public override string ToString()
    {
        return string.Format("{0}({1})", GetType().Name, Position);
    }
}
public abstract class DeclarationAst : StatementAst
{
    public string Identifier;
}
public abstract class TypeAst : DeclarationAst
{
}
public class SimpleTypeAst : TypeAst
{
}
public class ArrayTypeAst : TypeAst
{
    public IntegerLiteralExpressionAst Size;
}
public abstract class StatementAst : Ast
{
}
public class ProgramDeclarationAst : DeclarationAst
{
    public List<FunctionDeclarationAst> Functions =
        new List<FunctionDeclarationAst>();
    public BlockStatementAst MainBlock;
}
public class FunctionDeclarationAst : DeclarationAst
{
    public List<ParameterAst> Parameters =
        new List<ParameterAst>();
    public TypeAst Type;
    public BlockStatementAst BodyBlock;
}
public class ParameterAst : DeclarationAst
{
    public bool Var;
    public TypeAst Type;
}
public class VariableDeclarationAst : DeclarationAst
{
    public TypeAst Type;
}
public class VariableDeclarationStatementAst : StatementAst
{
    public List<VariableDeclarationAst> Declarations  =
        new List<VariableDeclarationAst>();
}
public class AssignmentStatementAst : StatementAst
{
    public ExpressionAst Variable;
    public ExpressionAst Value;
}
public class CallStatementAst : StatementAst
{
    public ExpressionAst Expression;
}
public class ReturnStatementAst : StatementAst
{
    public ExpressionAst Expression;
}
public abstract class StructuredStatementAst : StatementAst
{
}
public class BlockStatementAst : StructuredStatementAst
{
    public List<StatementAst> Statements =
        new List<StatementAst>();
}
public class IfStatementAst : StructuredStatementAst
{
    public ExpressionAst Condition;
    public StatementAst ThenBody;
    public StatementAst ElseBody;
}
public class WhileStatementAst : StructuredStatementAst
{
    public ExpressionAst Condition;
    public StatementAst Body;
}
public abstract class ExpressionAst : Ast
{
    public TypeAst Type;
}
public class StringLiteralExpressionAst : ExpressionAst
{
    public string Value;
    public string AsString()
    {
        return Value;
    }
}
public class IntegerLiteralExpressionAst : ExpressionAst
{
    public int Value;
    public string AsString()
    {
        return Value.ToString();
    }
}
public class RealLiteralExpressionAst : ExpressionAst
{
    public double Value;
    public string AsString()
    {
        return Value.ToString();
    }
}
public class VariableExpressionAst : ExpressionAst
{
    public string Identifier;
    public ExpressionAst Index;
}
public class UnaryExpressionAst : ExpressionAst
{
    public string Operator;
    public ExpressionAst Expression;
}
public class BinaryExpressionAst : ExpressionAst
{
    public ExpressionAst Left;
    public string Operator;
    public ExpressionAst Right;
}
public class CallExpressionAst : ExpressionAst
{
    public string Identifier;
    public List<ExpressionAst> Arguments =
        new List<ExpressionAst>();
}
class Parser
{
    public Parser(Scanner scanner)
    {
        _Scanner = scanner;
        _Scanner.Scan();
    }
    public ProgramDeclarationAst Parse()
    {
        ProgramDeclarationAst ret =
            new ProgramDeclarationAst { Position = _Position };
        _Require("program");
        ret.Identifier = _RequireIdentifier();
        _Require(";");
        bool parseFailure = false;
        while (_Current("procedure") || _Current("function"))
        {
            try
            {
                FunctionDeclarationAst function = _FunctionDeclaration();
                ret.Functions.Add(function);
            }
            catch (ParseException e)
            {
                Console.WriteLine("PANIC: {0}:\n    {1}",
                                  e.GetType().Name,
                                  e.Message);
                while (!(_Current(Tag.Eof) || _Current("begin")))
                {
                    _Scan();
                }
                Console.WriteLine("skip to: " + _Scanner);
                parseFailure = true;
            }
        }
        if (parseFailure)
        {
            throw new ParseException(
                string.Format(
                    "{0}:{1}: parser panicked",
                    _Scanner.Position.Line,
                    _Scanner.Position.Col));
        }
        ret.MainBlock = _BlockStatement();
        _Require(".");
        _Require(Tag.Eof);
        return ret;
    }
    Scanner _Scanner;
    Position _Position
    {
        get => _Scanner.Position;
    }
    FunctionDeclarationAst _FunctionDeclaration()
    {
        FunctionDeclarationAst ret =
            new FunctionDeclarationAst { Position = _Position };
        bool isFunction = false;
        if(_Current("function"))
        {
            isFunction = true;
        }
        if(!(_Consume("procedure") || _Consume("function")))
        {
            throw _Error();
        }
        ret.Identifier = _RequireIdentifier();
        _Require("(");
        if(!_Current(")"))
        {
            do
            {
                ParameterAst parameter =
                    new ParameterAst { Position = _Position };
                if(_Consume("var"))
                {
                    parameter.Var = true;
                }
                parameter.Identifier = _RequireIdentifier();
                _Require(":");
                parameter.Type = _Type();
                ret.Parameters.Add(parameter);
            } while(_Consume(","));
        }
        _Require(")");
        if(isFunction)
        {
            _Require(":");
            ret.Type = _Type();
        }
        else
        {
            ret.Type = null;
        }
        _Require(";");
        ret.BodyBlock = _BlockStatement();
        _Require(";");
        return ret;
    }
    TypeAst _Type()
    {
        if(_Current(Tag.Identifier))
        {
            return _SimpleType();
        }
        if(_Current("array"))
        {
            return _ArrayType();
        }
        throw _Error();
    }
    SimpleTypeAst _SimpleType()
    {
        SimpleTypeAst ret = new SimpleTypeAst { Position = _Position };
        ret.Identifier = _RequireIdentifier();
        return ret;
    }
    ArrayTypeAst _ArrayType()
    {
        ArrayTypeAst ret = new ArrayTypeAst { Position = _Position };
        _Require("array");
        _Require("[");
        ret.Size = _IntegerLiteralExpression();
        _Require("]");
        _Require("of");
        ret.Identifier = _RequireIdentifier();
        return ret;
    }
    StatementAst _Statement()
    {
        if(_Consume("var"))
        {
            VariableDeclarationStatementAst ret =
                new VariableDeclarationStatementAst { Position = _Position };
            ret.Declarations = new List<VariableDeclarationAst>();
            List<string> identifiers = new List<string>();
            do
            {
                identifiers.Add(_RequireIdentifier());
            } while (_Consume(","));
            _Require(":");
            TypeAst type = _Type();
            foreach(string identifier in identifiers)
            {
                VariableDeclarationAst declaration =
                    new VariableDeclarationAst { Position = _Position };
                declaration.Identifier = identifier;
                declaration.Type = type;
                ret.Declarations.Add(declaration);
            }
            return ret;
        }
        if(_Consume("return"))
        {
            ReturnStatementAst ret =
                new ReturnStatementAst { Position = _Position };
            ret.Expression = _Expression();
            return ret;
        }
        if(_Current("begin"))
        {
            StatementAst ret = _BlockStatement();
            _Require(";");
            return ret;
        }
        if(_Consume("if"))
        {
            IfStatementAst ret =
                new IfStatementAst { Position = _Position };
            ret.Condition = _Expression();
            _Require("then");
            ret.ThenBody = _Statement();
            if(_Consume("else"))
            {
                ret.ElseBody = _Statement();
            }
            return ret;
        }
        if(_Consume("while"))
        {
            WhileStatementAst ret =
                new WhileStatementAst { Position = _Position };
            ret.Condition = _Expression();
            _Require("do");
            ret.Body = _Statement();
            return ret;
        }
        if(_Current(Tag.Identifier))
        {
            ExpressionAst expression = _VariableOrCallExpression();
            if(!_Consume(":="))
            {
                CallStatementAst ret =
                    new CallStatementAst { Position = _Position };
                ret.Expression = expression;
                return ret;
            }
            {
                AssignmentStatementAst ret =
                    new AssignmentStatementAst { Position = _Position };
                ret.Variable = expression;
                ret.Value = _Expression();
                return ret;
            }
        }
        throw _Error();
    }
    BlockStatementAst _BlockStatement()
    {
        BlockStatementAst ret = new BlockStatementAst { Position = _Position };
        _Require("begin");
        while (!(_Current(Tag.Eof) || _Current("end")))
        {
            ret.Statements.Add(_Statement());
        }
        _Require("end");
        return ret;
    }
    ExpressionAst _Expression()
    {
        ExpressionAst left = _SimpleExpression();
        if(!(_Current("=") ||
             _Current("<>") ||
             _Current("<") ||
             _Current("<=") ||
             _Current(">=") ||
             _Current(">") ))
        {
            return left;
        }
        _Scan();
        BinaryExpressionAst ret =
            new BinaryExpressionAst { Position = _Position };
        ret.Left = left;
        ret.Operator = _Scanner.String;
        ret.Right = _FactorExpression();
        return ret;
    }
    ExpressionAst _VariableOrCallExpression()
    {
        string identifier = _RequireIdentifier();
        if(!_Consume("("))
        {
            VariableExpressionAst ret =
                new VariableExpressionAst { Position = _Position };
            ret.Identifier = identifier;
            if(_Consume("["))
            {
                ret.Index = _Expression();
                _Require("]");
            }
            return ret;
        }
        {
            CallExpressionAst ret = new CallExpressionAst { Position = _Position };
            ret.Identifier = identifier;
            if(!_Current(")"))
            {
                do
                {
                    ret.Arguments.Add(_Expression());
                } while (_Consume(","));
            }
            _Require(")");
            return ret;
        }
    }
    ExpressionAst _SimpleExpression()
    {
        bool isNegative = false;
        if(_Current("+") || _Current("-"))
        {
            if(_Consume("-"))
            {
                isNegative = true;
            }
            else
            {
                _Consume("+");
            }
        }
        ExpressionAst left = _TermExpression();
        if(!(_Current("+") ||
             _Current("-") ||
             _Current("or")))
        {
            return left;
        }
        BinaryExpressionAst ret =
            new BinaryExpressionAst { Position = _Position };
        ret.Left = left;
        ret.Operator = _Scanner.String;
        _Scan();
        ret.Right = _FactorExpression();
        return ret;
    }
    ExpressionAst _TermExpression()
    {
        ExpressionAst left = _FactorExpression();
        if(!(_Current("*") ||
             _Current("/") ||
             _Current("%") ||
             _Current("and")))
        {
            return left;
        }

        BinaryExpressionAst ret =
            new BinaryExpressionAst { Position = _Position };
        ret.Left = left;
        ret.Operator = _Scanner.String;
        _Scan();
        ret.Right = _FactorExpression();
        return ret;
    }
    ExpressionAst _FactorExpression()
    {
        if (_Consume("("))
        {
            ExpressionAst ret = _Expression();
            _Require(")");
            return ret;
        }
        if (_Consume("not"))
        {
            UnaryExpressionAst ret =
                new UnaryExpressionAst { Position = _Position };
            ret.Operator = "not";
            ret.Expression = _FactorExpression();
            return ret;
        }
        if(_Current(Tag.Identifier) ||
           _Current(Tag.StringLiteral) ||
           _Current(Tag.IntegerLiteral) ||
           _Current(Tag.RealLiteral))
        {
            if(_Current(Tag.Identifier))
            {
                return _VariableOrCallExpression();
            }
            if (_Current(Tag.StringLiteral))
            {
                return _StringLiteralExpression();
            }
            if (_Current(Tag.IntegerLiteral))
            {
                return _IntegerLiteralExpression();
            }
            if (_Current(Tag.RealLiteral))
            {
                return _RealLiteralExpression();
            }
        //             ExpressionAst ret = _FactorExpression();
        // _Require(".");
        // _Require("size");
        // return ret;
        }
        throw _Error();
    }
    StringLiteralExpressionAst _StringLiteralExpression()
    {
        StringLiteralExpressionAst ret =
            new StringLiteralExpressionAst { Position = _Position };
        _Consume(Tag.StringLiteral);
        ret.Value = _Scanner.String;
        return ret;
    }
    IntegerLiteralExpressionAst _IntegerLiteralExpression()
    {
        IntegerLiteralExpressionAst ret =
            new IntegerLiteralExpressionAst { Position = _Position };
        _Consume(Tag.IntegerLiteral);
        ret.Value = _Scanner.Integer;
        return ret;
    }
    RealLiteralExpressionAst _RealLiteralExpression()
    {
        RealLiteralExpressionAst ret =
            new RealLiteralExpressionAst { Position = _Position };
        _Consume(Tag.RealLiteral);
        ret.Value = _Scanner.Real;
        return ret;
    }
    Tag _Scan()
    {
        Console.WriteLine(_Scanner);
        return _Scanner.Scan();
    }
    bool _Current(string keyword)
    {
        if (_Scanner.Tag == Tag.Keyword && _Scanner.String == keyword)
        {
            return true;
        }
        return false;
    }
    bool _Current(Tag tag)
    {
        if (_Scanner.Tag == tag)
        {
            return true;
        }
        return false;
    }
    bool _Consume(string keyword)
    {
        if (_Current(keyword))
        {
            _Scan();
            return true;
        }
        return false;
    }
    bool _Consume(Tag tag)
    {
        if (_Current(tag))
        {
            _Scan();
            return true;
        }
        return false;
    }
    void _Require(string keyword)
    {
        if (_Current(keyword))
        {
            _Scan();
            return;
        }
        throw _Error(keyword);
    }
    void _Require(Tag tag)
    {
        if (_Current(tag))
        {
            _Scan();
            return;
        }
        throw _Error(tag);
    }
    string _RequireIdentifier()
    {
        if (_Current(Tag.Identifier))
        {
            string ret = _Scanner.String;
            _Scan();
            return ret;
        }
        throw _Error(Tag.Identifier);
    }
    ParseException _Error()
    {
        return new ParseException(
            string.Format(
                "function: {0}, token: ({1})",
                new StackFrame(1, true).GetMethod().Name,
                _Scanner));
    }
    ParseException _Error(string expected)
    {
        return new ParseException(
            string.Format(
                "function: {0}, token: ({1}), expected: {2}",
                new StackFrame(1, true).GetMethod().Name,
                _Scanner,
                expected));
    }
    ParseException _Error(Tag expected = Tag.Error)
    {
        return _Error(expected.ToString());
    }
}
public abstract class AstVisitor
{
    public virtual void Visit(Ast ast)
    {
        Visit(ast as dynamic);
    }
    public virtual void Visit(ParameterAst ast)
    {
    }
    public virtual void Visit(SimpleTypeAst ast)
    {
    }
    public virtual void Visit(ArrayTypeAst ast)
    {
    }
    public virtual void Visit(ProgramDeclarationAst ast)
    {
    }
    public virtual void Visit(FunctionDeclarationAst ast)
    {
    }
    public virtual void Visit(VariableDeclarationAst ast)
    {
    }
    public virtual void Visit(VariableDeclarationStatementAst ast)
    {
    }
    public virtual void Visit(AssignmentStatementAst ast)
    {
    }
    public virtual void Visit(CallStatementAst ast)
    {
    }
    public virtual void Visit(ReturnStatementAst ast)
    {
    }
    public virtual void Visit(BlockStatementAst ast)
    {
    }
    public virtual void Visit(IfStatementAst ast)
    {
    }
    public virtual void Visit(WhileStatementAst ast)
    {
    }
    public virtual void Visit(StringLiteralExpressionAst ast)
    {
    }
    public virtual void Visit(IntegerLiteralExpressionAst ast)
    {
    }
    public virtual void Visit(RealLiteralExpressionAst ast)
    {
    }
    public virtual void Visit(VariableExpressionAst ast)
    {
    }
    public virtual void Visit(UnaryExpressionAst ast)
    {
    }
    public virtual void Visit(BinaryExpressionAst ast)
    {
    }
    public virtual void Visit(CallExpressionAst ast)
    {
    }
}
public class AstPrinter : AstVisitor
{
    public override void Visit(SimpleTypeAst ast)
    {
        _TreePrint(ast.Identifier);
    }
    public override void Visit(ArrayTypeAst ast)
    {
        _TreePrint("array [" +
                  ast.Size.Value +
                  "] of " +
                  ast.Identifier);
    }
    public override void Visit(ProgramDeclarationAst ast)
    {
        _TreePrint(ast);
        foreach(FunctionDeclarationAst statement in ast.Functions)
        {
            Visit(statement);
        }
        Visit(ast.MainBlock);
    }
    public override void Visit(FunctionDeclarationAst ast)
    {
        _TreePrint(ast);
        foreach(ParameterAst parameter in ast.Parameters)
        {
            Visit(parameter);
        }
        if(ast.Type != null)
        {
            Visit(ast.Type);
        }
        Visit(ast.BodyBlock);
    }
    public override void Visit(ParameterAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        if(ast.Var)
        {
            _TreePrint("var");
        }
        _TreePrint(ast.Identifier);
        Visit(ast.Type);
        _Depth--;
    }
    public override void Visit(VariableDeclarationAst ast)
    {
        _TreePrint(ast);
        _TreePrint(ast.Identifier);
        Visit(ast.Type);
    }
    public override void Visit(VariableDeclarationStatementAst ast)
    {
        _TreePrint(ast);
        foreach(VariableDeclarationAst declaration in ast.Declarations)
        {
            Visit(declaration);
        }
    }
    public override void Visit(AssignmentStatementAst ast)
    {
        _TreePrint(ast);
        Visit(ast.Variable);
        Visit(ast.Value);
    }
    public override void Visit(CallStatementAst ast)
    {
        _TreePrint(ast);
        Visit(ast.Expression);
    }
    public override void Visit(ReturnStatementAst ast)
    {
        _TreePrint(ast);
        Visit(ast.Expression);
    }
    public override void Visit(BlockStatementAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        foreach(StatementAst statement in ast.Statements)
        {
            Visit(statement);
        }
        _Depth--;
    }
    public override void Visit(IfStatementAst ast)
    {
        _TreePrint(ast);
        Visit(ast.Condition);
        _Depth++;
        Visit(ast.ThenBody);
        Visit(ast.ElseBody);
        _Depth--;
    }
    public override void Visit(WhileStatementAst ast)
    {
        _TreePrint(ast);
        Visit(ast.Condition);
        _Depth++;
        Visit(ast.Body);
        _Depth--;
    }
    public override void Visit(StringLiteralExpressionAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        _TreePrint(ast.AsString());
        _Depth--;
    }
    public override void Visit(IntegerLiteralExpressionAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        _TreePrint(ast.AsString());
        _Depth--;
    }
    public override void Visit(RealLiteralExpressionAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        _TreePrint(ast.AsString());
        _Depth--;
    }
    public override void Visit(VariableExpressionAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        _TreePrint(ast.Identifier);
        _Depth--;
    }
    public override void Visit(UnaryExpressionAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        _TreePrint(ast.Operator);
        Visit(ast.Expression);
        _Depth--;
    }
    public override void Visit(BinaryExpressionAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        Visit(ast.Left);
        _TreePrint(ast.Operator);
        Visit(ast.Right);
        _Depth--;
    }
    public override void Visit(CallExpressionAst ast)
    {
        _TreePrint(ast);
        _Depth++;
        _TreePrint(ast.Identifier);
        foreach(ExpressionAst argument in ast.Arguments)
        {
            Visit(argument);
        }
        _Depth--;
    }
    int _Depth = 0;
    void _TreePrint(Ast ast)
    {
        _TreePrint(ast as dynamic);
    }
    void _TreePrint(ParameterAst ast)
    {
        Console.WriteLine("{0}{1}",
                          new String(' ', (_Depth + 1) * 4), ast);
    }
    void _TreePrint(TypeAst ast)
    {
        Console.WriteLine("{0}{1}",
                          new String(' ', (_Depth + 1) * 4), ast);
    }
    void _TreePrint(StatementAst ast)
    {
        Console.WriteLine("{0}{1}",
                          new String(' ', _Depth * 4), ast);
    }
    void _TreePrint(ExpressionAst ast)
    {
        Console.WriteLine("{0}{1}",
                          new String(' ', (_Depth + 1) * 4), ast);
    }
    void _TreePrint(string s)
    {
        Console.WriteLine("{0}{1}",
                          new String(' ', (_Depth + 1) * 4), s);
    }
}
public class SymbolTable : AstVisitor
{
    public Dictionary<(int, string), DeclarationAst> Symtab =
        new Dictionary<(int, string), DeclarationAst>();
    public override void Visit(ProgramDeclarationAst ast)
    {
        _Add(new SimpleTypeAst { Identifier = "boolean" });
        _Add(new VariableDeclarationAst { Identifier = "false" });
        _Add(new SimpleTypeAst { Identifier = "integer" });
        _Add(new FunctionDeclarationAst { Identifier = "read" });
        _Add(new SimpleTypeAst { Identifier = "real" });
        _Add(new SimpleTypeAst { Identifier = "string" });
        _Add(new VariableDeclarationAst { Identifier = "true" });
        _Add(new FunctionDeclarationAst { Identifier = "writeln" });
        _Scope++;
        _Add(ast);
        foreach(FunctionDeclarationAst statement in ast.Functions)
        {
            Visit(statement);
        }
        _Scope++;
        Visit(ast.MainBlock);
    }
    public override void Visit(FunctionDeclarationAst ast)
    {
        _Add(ast);
        _Scope++;
        foreach(ParameterAst parameter in ast.Parameters)
        {
            _Add(parameter);
        }
        Visit(ast.BodyBlock);
    }
    public override void Visit(BlockStatementAst ast)
    {
        foreach(StatementAst statement in ast.Statements)
        {
            Visit(statement);
        }
    }
    public override void Visit(IfStatementAst ast)
    {
        _Scope++;
        Visit(ast.Condition);
        Visit(ast.ThenBody);
        Visit(ast.ElseBody);
    }
    public override void Visit(WhileStatementAst ast)
    {
        _Scope++;
        Visit(ast.Condition);
        Visit(ast.Body);
    }
    public override void Visit(VariableDeclarationAst ast)
    {
        _Add(ast);
    }
    public override void Visit(VariableDeclarationStatementAst ast)
    {
        foreach(VariableDeclarationAst declaration in ast.Declarations)
        {
            Visit(declaration);
        }
    }
    int _Scope = 0;
    void _Add(DeclarationAst ast)
    {
        Console.WriteLine(_Scope + ": " + ast.Identifier);
        Symtab.Add((_Scope, ast.Identifier), ast);
    }
}
public class TypeChecker : AstVisitor
{
    public TypeChecker(SymbolTable symtab)
    {
        _Symtab = symtab;
    }
    public override void Visit(ProgramDeclarationAst ast)
    {
        _PushScope();
        _PushScope();
        foreach(FunctionDeclarationAst statement in ast.Functions)
        {
            Visit(statement);
        }
        _PushScope();
        Visit(ast.MainBlock);
    }
    public override void Visit(FunctionDeclarationAst ast)
    {
        _PushScope();
        foreach(ParameterAst parameter in ast.Parameters)
        {
            Visit(parameter);
        }
        Visit(ast.BodyBlock);
    }
    public override void Visit(ParameterAst ast)
    {
        Visit(ast.Type);
    }
    public override void Visit(SimpleTypeAst ast)
    {
        if(!(ast.Identifier == "string" ||
             ast.Identifier == "integer" ||
             ast.Identifier == "real" ||
             ast.Identifier == "boolean"))
        {
            throw new TypeException(ast);
        }
    }
    public override void Visit(ArrayTypeAst ast)
    {
        if(ast.Identifier != "integer")
        {
            throw new TypeException(ast, "integer");
        }
    }
    public override void Visit(BlockStatementAst ast)
    {
        foreach(StatementAst statement in ast.Statements)
        {
            Visit(statement);
        }
    }
    public override void Visit(IfStatementAst ast)
    {
        _PushScope();
        Visit(ast.Condition);
        Visit(ast.ThenBody);
        Visit(ast.ElseBody);
    }
    public override void Visit(WhileStatementAst ast)
    {
        _PushScope();
        Visit(ast.Condition);
        Visit(ast.Body);
    }
    public override void Visit(VariableDeclarationAst ast)
    {
        Visit(ast.Type);
    }
    public override void Visit(VariableDeclarationStatementAst ast)
    {
        foreach(VariableDeclarationAst declaration in ast.Declarations)
        {
            Visit(declaration);
        }
    }
    public override void Visit(AssignmentStatementAst ast)
    {
        if(Type(ast.Variable) != Type(ast.Value))
        {
            throw new TypeException(ast.Value, Type(ast.Variable));
        }
    }
    public override void Visit(CallStatementAst ast)
    {
        Visit(ast.Expression);
    }
    public override void Visit(CallExpressionAst ast)
    {
        // FunctionDeclarationAst func = _Symtab
        foreach(ExpressionAst argument in ast.Arguments)
        {

        }
    }
    public string Type(ExpressionAst ast)
    {
        return Type(ast as dynamic);
    }
    public string Type(StringLiteralExpressionAst ast)
    {
        return "string";
    }
    public string Type(IntegerLiteralExpressionAst ast)
    {
        return "integer";
    }
    public string Type(RealLiteralExpressionAst ast)
    {
        return "real";
    }
    public string Type(VariableExpressionAst ast)
    {
        return Type(ast.Identifier);
    }
    public string Type(string ident)
    {
        return "fuck";//_SymTab[ident];
    }
    public string Type(BinaryExpressionAst ast)
    {
        // if((ast.Operator == "+" ||
        //    ast.Operator == "-" ||
        //     ast.Operator == "*" ||
        //     ast.Operator == "/" ) &&
        //    (ast.))
        // {
        // }


// VarType lht = Type(ast.Lhs);
        // VarType rht = Type(ast.Rhs);
        // if (lht == VarType.Int &&
        //     rht == VarType.Int &&
        //     (ast.Op == Tag.AddOp ||
        //      ast.Op == Tag.SubOp ||
        //      ast.Op == Tag.MulOp ||
        //      ast.Op == Tag.DivOp))
        //     return VarType.Int;
        // else if (lht == VarType.String &&
        //          rht == VarType.String &&
        //          ast.Op == Tag.AddOp)
        //     return VarType.String;
        // else if (lht == VarType.Bool &&
        //          rht == VarType.Bool &&
        //          ast.Op == Tag.AndOp ||
        //          (lht == rht && (ast.Op == Tag.EqOp || ast.Op == Tag.LtOp)))
        //     return VarType.Bool;
        // throw new TypeException(ast);
        return "todo";
    }
    public string Type(UnaryExpressionAst ast)
    {
        if(Type(ast.Expression) != "boolean")
        {
            throw new TypeException(ast.Expression, "boolean");
        }
        return "boolean";
    }
    public string Type(CallExpressionAst ast)
    {
        return "todo";
                // foreach(ExpressionAst argument in ast.Arguments)
        // {
        //     Visit(argument);
        // }

    }
    int _Scope = -1;
    List<int> _ScopeStack = new List<int>();
    SymbolTable _Symtab;
    void _PushScope()
    {
        _Scope++;
        _ScopeStack.Add(_Scope);
    }
    void _PopScope()
    {
        _Scope--;
        _ScopeStack.RemoveAt(_ScopeStack.Count - 1);
    }
    DeclarationAst _Find(string identifier)
    {
        for(int i = _ScopeStack.Count - 1; i >= 0; i--)
        {
            if(_Symtab.Symtab.ContainsKey((_Scope, identifier)))
            {
                return _Symtab.Symtab[(_Scope, identifier)];
            }
        }
        return null;
    }
}
public class CompilerException : Exception
{
    public CompilerException(string message) : base(message) { }
}
public class ParseException : CompilerException
{
    public ParseException(string message) : base(message) { }
}
class TypeException : CompilerException
{
    public TypeException(Ast ast) :
    base(string.Format("wrong type: {0}", ast)) { }
    public TypeException(Ast ast, string expected) :
    base(string.Format("wrong type: {0}, expected: {1}", ast, expected)) { }
}
class MainClass
{
    static void EvalFile(string path)
    {
        string text = System.IO.File.ReadAllText(path);
        Console.WriteLine("Scanner:");
        Console.WriteLine("------------------------------------------");
        Scanner scanner = new Scanner(new System.IO.StringReader(text));
        while(scanner.Scan() != Tag.Eof)
        {
            Console.WriteLine(scanner);
        }
        Console.WriteLine("------------------------------------------");
        Console.WriteLine("Parser:");
        Console.WriteLine("------------------------------------------");
        scanner = new Scanner(new System.IO.StringReader(text));
        Parser parser = new Parser(scanner);
        ProgramDeclarationAst program = parser.Parse();
        Console.WriteLine("------------------------------------------");
        Console.WriteLine("Syntax tree:");
        Console.WriteLine("------------------------------------------");
        new AstPrinter().Visit(program);
        Console.WriteLine("------------------------------------------");
        Console.WriteLine("Symbol table:");
        Console.WriteLine("------------------------------------------");
        SymbolTable symtab = new SymbolTable();
        symtab.Visit(program);
        Console.WriteLine("------------------------------------------");
        Console.WriteLine("Type checker: ");
        Console.WriteLine("------------------------------------------");
        new TypeChecker(symtab).Visit(program);
        Console.WriteLine("OK");
        Console.WriteLine("------------------------------------------");
        Console.WriteLine("Code generation:");
        Console.WriteLine("------------------------------------------");
        // new AstEvaluator().Eval(stmtList);
        Console.WriteLine();
        Console.WriteLine("------------------------------------------");
    }
    static void Main(string[] args)
    {
        if (args.Length != 1)
        {
            Console.WriteLine(
                "usage: {0} filename",
                System.Diagnostics.Process.GetCurrentProcess().ProcessName);
        }
        else
        {
            EvalFile(args[0]);
        }
    }
}
}
