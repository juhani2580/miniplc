﻿using System;
using NUnit.Framework;
using Minipl;
namespace MiniplTest
{
    [TestFixture()]
    public class Test
    {
        [TestCase("123", 123)]
        public void IntegerLiteral(string value, int expected)
        {
            Scanner scanner = new Scanner(new System.IO.StringReader(value));
            scanner.Scan();
            Assert.True(scanner.Integer == expected);
            Assert.True(scanner.Tag == Tag.IntegerLiteral);
        }
        // [TestCase("123.4")]
        // [TestCase("123.4e5")]
        // [TestCase("123.4e+5")]
        // [TestCase("123.4e-5")]
        // public void Real(string value)
        // {
        //     Scanner scanner = new Scanner(new System.IO.StringReader(value));
        //     scanner.Scan();
        //     Assert.True(scanner.Tag == Tag.Real);
        // }
        [TestCase("+")]
        [TestCase("-")]
        [TestCase("*")]
        [TestCase("%")]
        [TestCase("=")]
        [TestCase("<>")]
        [TestCase("<")]
        [TestCase(">")]
        [TestCase("<=")]
        [TestCase(">=")]
        [TestCase("(")]
        [TestCase(")")]
        [TestCase("[")]
        [TestCase("]")]
        [TestCase(".")]
        [TestCase(",")]
        [TestCase(":")]
        [TestCase(";")]
        [TestCase("or")]
        [TestCase("and")]
        [TestCase("not")]
        [TestCase("if")]
        [TestCase("then")]
        [TestCase("else")]
        [TestCase("of")]
        [TestCase("while")]
        [TestCase("do")]
        [TestCase("begin")]
        [TestCase("end")]
        [TestCase("var")]
        [TestCase("array")]
        [TestCase("procedure")]
        [TestCase("function")]
        [TestCase("program")]
        [TestCase("return")]
        public void Keyword(string value)
        {
            Scanner scanner = new Scanner(new System.IO.StringReader(value));
            scanner.Scan();
            Assert.True(scanner.String == value);
            Assert.True(scanner.Tag == Tag.Keyword);
        }
        [TestCase("my_id")]
        [TestCase("Boolean")]
        public void Identifier(string value)
        {
            Scanner scanner = new Scanner(new System.IO.StringReader(value));
            scanner.Scan();
            Assert.True(scanner.String == value.ToLower());
            Assert.True(scanner.Tag == Tag.Identifier);
        }
    }
}
