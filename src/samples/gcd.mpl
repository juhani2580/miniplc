program gcd_program;

function gcd(a : integer, b : integer) : integer;
begin
    var tmp : integer
    while b <> 0 do
    begin
        tmp := b
        b := a % b
        a := tmp
    end;
    return a
end;

begin
    var a, b : int
    read(a)
    read(b)
	writeln(gcd(a, b))
end.