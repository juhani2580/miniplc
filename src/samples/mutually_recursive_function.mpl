program mutually_recursive_function_program;

function gcd(a, b : integer);
begin
    var tmp : integer
    while b <> 0 do
    begin
        tmp := b
        b := a mod b
        a := tmp
    end;
    return a
end;

begin
    var a, b : int
    read(a)
    read(b)
	writeln(gcd(a, b));
end.